package com.citysavings.online_turnoverapplication.retrofit;

import com.citysavings.online_turnoverapplication.models.Auth_Model;
import com.citysavings.online_turnoverapplication.retrofit.model.Auth_Resp_Model;
import com.citysavings.online_turnoverapplication.retrofit.model.Branch_Resp_Model;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiInterface {
    @POST("api/Authentication/Accesstoken")
    Call<Auth_Resp_Model> authorization (@Body Auth_Model value);

    @GET("api/Mstr_BranchCode?$select=code,region,title&$orderby=title")
    Call<List<Branch_Resp_Model>> getBranch (@Header("Authorization") String token);
}
