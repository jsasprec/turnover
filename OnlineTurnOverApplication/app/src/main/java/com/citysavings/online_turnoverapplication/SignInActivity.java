package com.citysavings.online_turnoverapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.auth0.android.jwt.JWT;
import com.citysavings.online_turnoverapplication.models.Auth_Model;
import com.citysavings.online_turnoverapplication.retrofit.Api;
import com.citysavings.online_turnoverapplication.retrofit.model.Auth_Resp_Model;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends AppCompatActivity {

    @BindView(R.id.input_employeeID)
    TextInputEditText inputEmployeeID;
    @BindView(R.id.input_password)
    TextInputEditText inputPassword;
    @BindView(R.id.button_signin)
    Button buttonSignin;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        sharedPreferences = getSharedPreferences(getString(R.string.spName), Context.MODE_PRIVATE);
    }

    @OnClick(R.id.button_signin)
    public void onViewClicked() {
        try {
            Auth_Model x = new Auth_Model();
            x.employeeID = inputEmployeeID.getText().toString();
            x.password = inputPassword.getText().toString();

            Call<Auth_Resp_Model> call = Api.apiInterface().authorization(x);
            call.enqueue(new Callback<Auth_Resp_Model>() {
                @Override
                public void onResponse(Call<Auth_Resp_Model> call, Response<Auth_Resp_Model> response) {
                    if (response.code() == 201) {
                        Toast.makeText(SignInActivity.this, response.body().getToken() + "", Toast.LENGTH_SHORT).show();
                        final SharedPreferences.Editor editor = sharedPreferences.edit();

                        editor.putString("token", response.body().getToken());

                        JWT jwt = new JWT(response.body().getToken());
                        editor.putString("role", jwt.getClaim("roles").asString());

                        editor.commit();
                        editor.apply();

                        Intent intent = new Intent(SignInActivity.this, MainListActivity.class);
                        SignInActivity.this.startActivity(intent);

                    } else {
                        Toast.makeText(SignInActivity.this, response.code() + ": Please check your credential and try again.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Auth_Resp_Model> call, Throwable t) {
                    Toast.makeText(SignInActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        String token = sharedPreferences.getString("token", null);
        if (token !=null) {
           JWT jwt = new JWT(token);
           if(!jwt.isExpired(0))
           {
               Intent intent = new Intent(this, MainListActivity.class);
               this.startActivity(intent);
               finish();
           }
           else
           {
               Toast.makeText(this, "Token Expired.", Toast.LENGTH_SHORT).show();
           }
        }
        else
            Toast.makeText(this, "No Token.", Toast.LENGTH_SHORT).show();
    }
}
