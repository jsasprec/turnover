package com.citysavings.online_turnoverapplication;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.citysavings.online_turnoverapplication.retrofit.Api;
import com.citysavings.online_turnoverapplication.retrofit.model.Branch_Resp_Model;
import com.citysavings.online_turnoverapplication.room.ContextDB;
import com.citysavings.online_turnoverapplication.room.adapter.adap_branch;
import com.citysavings.online_turnoverapplication.room.entities.ent_branch;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainListActivity extends AppCompatActivity {

    ContextDB contextDB;
    //adap_profile adapProfile;
    adap_branch adapBranch;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.fb_Add)
    FloatingActionButton fbAdd;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.textViewNodata)
    TextView textViewNodata;

    SharedPreferences sharedPreferences;
    Boolean executing = false;
    Boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_list);
        ButterKnife.bind(this);

        contextDB = ContextDB.getAppDatabase(this);
        //Todo Change adapter to adapBranch

        adapBranch = new adap_branch(this, new ArrayList<>());


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapBranch);
        recyclerView.smoothScrollToPosition(adapBranch.getItemCount());

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1)) {
                    fbAdd.hide();

                } else {
                    fbAdd.show();
                }
            }
        });

        swiperefresh.setOnRefreshListener(() -> {
            if (!executing) {
                fetchData();
                swiperefresh.setRefreshing(false);
            }
        });

        sharedPreferences = getSharedPreferences(getString(R.string.spName), Context.MODE_PRIVATE);

        fetchData();

    }

    public void fetchData() {
        try {
            executing = true;
            Call<List<Branch_Resp_Model>> call = Api.apiInterface().getBranch("Bearer " + sharedPreferences.getString("token", ""));
            call.enqueue(new Callback<List<Branch_Resp_Model>>() {

                @Override
                public void onResponse(Call<List<Branch_Resp_Model>> call, Response<List<Branch_Resp_Model>> response) {
                    if (response.code() == 200) {
                        //Toast.makeText(MainListActivity.this, response.body().size() + "", Toast.LENGTH_SHORT).show();

                        for (Branch_Resp_Model x : response.body()) {
                            ent_branch ent = new ent_branch();
                            ent.code = x.getCode();
                            ent.region = x.getRegion();
                            ent.title = x.getTitle();
                            ent_branch existEnt = contextDB.dao_branch().getById(x.getCode());
                            if (existEnt == null) {
                                contextDB.dao_branch().insert(ent);
                            }
                        }
                        refreshRV();

                    } else {
                        Toast.makeText(MainListActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<List<Branch_Resp_Model>> call, Throwable t) {
                    Toast.makeText(MainListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

            executing = false;
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            executing = false;
        }
    }

    @OnClick(R.id.fb_Add)
    public void onViewClicked() {
        Intent intent = new Intent(this, FragmentHolderActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshRV();
    }

    private void refreshRV() {
        try {

            adapBranch.insertItems(contextDB.dao_branch().getAll());

            if (adapBranch.getItemCount() > 0)
                textViewNodata.setVisibility(View.GONE);
            else
                textViewNodata.setVisibility(View.VISIBLE);

            recyclerView.setAdapter(adapBranch);
            //recyclerView.smoothScrollToPosition(adapBranch.getItemCount());

        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.appbarmeny, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_item_logout) {
            logOutDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(doubleBackToExitPressedOnce)
        {
            logOutDialog();
            return;
        }

        this.doubleBackToExitPressedOnce = true;

        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    public void logOutDialog() {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Do you want to LogOut?")
                    .setTitle("Warning");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    final SharedPreferences.Editor editor = sharedPreferences.edit();

                    editor.clear();
                    editor.apply();
                    editor.commit();
                    Intent intent = new Intent(MainListActivity.this, SignInActivity.class);
                    MainListActivity.this.startActivity(intent);
                    finish();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
