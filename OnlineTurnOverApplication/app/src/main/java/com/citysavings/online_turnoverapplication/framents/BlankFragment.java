package com.citysavings.online_turnoverapplication.framents;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.citysavings.online_turnoverapplication.FragmentHolderActivity;
import com.citysavings.online_turnoverapplication.R;
import com.citysavings.online_turnoverapplication.room.ContextDB;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BlankFragment extends Fragment {

    @BindView(R.id.textViewRole)
    TextView textViewRole;
    @BindView(R.id.imageView)
    ImageView imageView;
    ContextDB contextDB;
    FragmentHolderActivity activity;
    public BlankFragment(ContextDB _contextDB, FragmentHolderActivity _activity) {
        // Required empty public constructor
        contextDB = _contextDB;
        activity = _activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank2, container, false);
        ButterKnife.bind(this, view);


        int sample = contextDB.dao_branch().getAll().size();

        Toast.makeText(activity, sample + " Sample", Toast.LENGTH_SHORT).show();

        Bundle mBundle = getArguments();

        textViewRole.setText(mBundle.getString("role"));

        activity.sample = "Andito";

        return view;
    }
}
