package com.citysavings.online_turnoverapplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.citysavings.online_turnoverapplication.framents.BlankFragment;
import com.citysavings.online_turnoverapplication.room.ContextDB;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentHolderActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    @BindView(R.id.frame1)
    FrameLayout frame1;
    ContextDB contextDB;
    public String sample = "n/a";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_holder);
        ButterKnife.bind(this);
        sharedPreferences = getSharedPreferences(getString(R.string.spName), Context.MODE_PRIVATE);
        contextDB = ContextDB.getAppDatabase(this);

        try {
            Bundle bundle = new Bundle();
            String role = sharedPreferences.getString("role", null);
            bundle.putString("role", role);

            Fragment fragment = new BlankFragment(contextDB, this);
            fragment.setArguments(bundle);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);

            ft.replace(frame1.getId(), fragment);
            //ft.addToBackStack(null);
            ft.commit();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getCause().toString(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, sample, Toast.LENGTH_SHORT).show();
    }
}
