package com.citysavings.online_turnoverapplication.room.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.citysavings.online_turnoverapplication.MainActivity;
import com.citysavings.online_turnoverapplication.R;
import com.citysavings.online_turnoverapplication.room.entities.ent_profile;

import java.util.List;

public class adap_profile  extends RecyclerView.Adapter<adap_profile.MyViewHolder>  {

    Context context;
    List<ent_profile> collection;

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        private TextView fullName,email,phone;

        public MyViewHolder(View view) {
            super(view);
            fullName = view.findViewById(R.id.textView_rv_fullName);
            email = view.findViewById(R.id.textView_rv_email);
            phone = view.findViewById(R.id.textView_rv_phone);
        }

    }
    public adap_profile(Context _context, List<ent_profile> _collection)
    {
        context = _context;
        collection = _collection;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_item, parent, false);

        return new MyViewHolder(itemView);
    }

    public void insertItems(List<ent_profile> profiles)
    {
        collection = profiles;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final ent_profile profile = collection.get(position);
        holder.fullName.setText(profile.getFullName());
        holder.email.setText(profile.getEmail());
        holder.phone.setText(profile.getPhone());

        holder.itemView.setOnClickListener(v -> {
            Intent i = new Intent(context.getApplicationContext(), MainActivity.class);
            i.putExtra("key",profile.key);
            context.startActivity(i);
        });
    }

    @Override
    public int getItemCount() {
        return collection.size();
    }

}
