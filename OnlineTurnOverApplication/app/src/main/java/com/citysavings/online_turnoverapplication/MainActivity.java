package com.citysavings.online_turnoverapplication;

import android.icu.text.CaseMap;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.citysavings.online_turnoverapplication.room.ContextDB;
import com.citysavings.online_turnoverapplication.room.entities.ent_profile;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.editTextFullName)
    EditText editTextFullName;
    @BindView(R.id.editTextSecretCode)
    EditText editTextSecretCode;
    @BindView(R.id.switchSecretCode)
    Switch switchSecretCode;
    @BindView(R.id.textInputEmail)
    TextInputEditText textInputEmail;
    @BindView(R.id.textInputPhone)
    TextInputEditText textInputPhone;
    @BindView(R.id.buttonSave)
    Button buttonSave;


    private static ContextDB contextDB;
    private ent_profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setTitle("Personal Information Form");

        contextDB = ContextDB.getAppDatabase(this);

        Bundle x = getIntent().getExtras();

        if (x != null) {
            if (!x.getString("key", null).isEmpty()) {
                profile = contextDB.dao_profile().getById(x.getString("key", null));
                editTextFullName.setText(profile.getFullName());
                editTextSecretCode.setText(profile.getSecretCode());
                textInputEmail.setText(profile.getEmail());
                textInputPhone.setText(profile.getPhone());
            }
        }
    }

    @OnClick(R.id.switchSecretCode)
    public void onSwitchSecretCodeClicked() {
        if (switchSecretCode.isChecked())
            editTextSecretCode.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        else
            editTextSecretCode.setTransformationMethod(PasswordTransformationMethod.getInstance());
    }

    @OnClick(R.id.buttonSave)
    public void onButtonSaveClicked() {
        try {
            //Validation

            //Fill data into entities
            ent_profile data = new ent_profile();

            data.Email = textInputEmail.getText().toString();
            data.FullName = editTextFullName.getText().toString();
            data.Phone = textInputPhone.getText().toString();
            data.SecretCode = editTextSecretCode.getText().toString();

            //Insert data using the dao in our db context
            if (profile == null) {
                data.key = UUID.randomUUID().toString();
                contextDB.dao_profile().insert(data);
            } else {
                data.key = profile.key;
                contextDB.dao_profile().update(data);
            }


            //checking if the data exist use the getby id from the uid created in our entity
            ent_profile firstProfile = contextDB.dao_profile().getById(data.key);

            //Toast the result
            Toast.makeText(this, "Generated Key: " + firstProfile.key + " for " + firstProfile.getKey(), Toast.LENGTH_LONG).show();
            finish();

        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
