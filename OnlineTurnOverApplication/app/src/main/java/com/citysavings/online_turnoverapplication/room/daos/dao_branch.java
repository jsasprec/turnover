package com.citysavings.online_turnoverapplication.room.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.citysavings.online_turnoverapplication.room.entities.ent_branch;

import java.util.List;

@Dao
public interface dao_branch {
    @Insert
    void insert(ent_branch... entity);

    @Update
    void update(ent_branch... entity);

    @Delete
    void dalete(ent_branch... entity);

    @Query("Delete from ent_branch")
    void deleteAll();

    @Query("Select * FROM ent_branch where `code`=:code")
    ent_branch getById(String code);

    @Query("Select * FROM ent_branch")
    List<ent_branch> getAll();

    @Query("select * from ent_branch order by title")
    LiveData<List<ent_branch>> getLiveAll();
}
