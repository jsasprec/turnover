package com.citysavings.online_turnoverapplication.room.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.citysavings.online_turnoverapplication.R;
import com.citysavings.online_turnoverapplication.room.entities.ent_branch;

import java.util.List;

public class adap_branch   extends RecyclerView.Adapter<adap_branch.MyViewHolder>  {

    Context context;
    List<ent_branch> collection;

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        private TextView title,code,region;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.textView_rv_title);
            code = view.findViewById(R.id.textView_rv_code);
            region = view.findViewById(R.id.textView_rv_region);
        }

    }
    public adap_branch(Context _context, List<ent_branch> _collection)
    {
        context = _context;
        collection = _collection;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rc_item_branch, parent, false);

        return new MyViewHolder(itemView);
    }

    public void insertItems(List<ent_branch> branches)
    {
        collection = branches;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final ent_branch profile = collection.get(position);
        holder.title.setText(profile.getTitle());
        holder.code.setText(profile.getCode());
        holder.region.setText(profile.getRegion());
    }

    @Override
    public int getItemCount() {
        return collection.size();
    }

}
