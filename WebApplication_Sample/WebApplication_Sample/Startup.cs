﻿using ClassLibrary_Sample;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNet.OData.Formatter;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApplication_Sample.EF;

namespace WebApplication_Sample
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOData();

            services.Configure<IISServerOptions>(options =>
            {
                options.AutomaticAuthentication = false;
            });
            services.AddMvc(
                op =>
                {
                    foreach (var formatter in op.OutputFormatters
                        .OfType<ODataOutputFormatter>()
                        .Where(it => !it.SupportedMediaTypes.Any()))
                    {
                        formatter.SupportedMediaTypes.Add(
                            new MediaTypeHeaderValue("application/prs.mock-odata"));
                    }
                    foreach (var formatter in op.InputFormatters
                        .OfType<ODataInputFormatter>()
                        .Where(it => !it.SupportedMediaTypes.Any()))
                    {
                        formatter.SupportedMediaTypes.Add(
                            new MediaTypeHeaderValue("application/prs.mock-odata"));
                    }
                }
                ).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(Configuration["Version"], new Info { Title = Configuration["Title"], Version = Configuration["Version"] });

                var security = new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[] { }},
                };
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    { "Bearer", new string[] { } }
                });
            });

            services.AddDbContext<MyContext>(options =>
              options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            #region Jwt

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidAudience = Configuration["Jwt:audience"],
                    ValidIssuer = Configuration["Jwt:issuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.GetSection("Jwt:jwt_key").Get<string>())),
                    ClockSkew = TimeSpan.Zero,
                    ValidateLifetime = true
                };
            });

            services.AddAuthorization(opt =>
            {
                var responses = Configuration
                    .GetSection("Jwt:policy")
                    .Get<AppSettings>();

                foreach (PolicyMapping section in responses.PolicyMapping)
                {
                    var policyLst = new List<string>(section.Policy);
                    var roleLst = new List<string>(section.Role);

                    opt.AddPolicy(section.Title,
                    policy => policy
                        .RequireClaim("policy", policyLst)
                        .RequireRole(roleLst));
                }
            });

            #endregion Jwt

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, MyContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint($"{Configuration["swag_url"]}", $"{Configuration["Title"]} : {Configuration["Version"]}");
                    c.DocumentTitle = Configuration["Title"];
                    c.DocExpansion(DocExpansion.None);
                });
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseAuthentication();
            app.UseHttpsRedirection();

            app.UseMvc(b =>
            {
                b.EnableDependencyInjection();
                b.Select().OrderBy().Count().Filter().MaxTop(100);
            });
            
            app.UseStaticFiles();

            DbContextIntializer.Initialize(context, env);
        }
    }
}