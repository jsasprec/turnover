﻿using ClassLibrary_Sample;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.IO;
using System.Linq;

namespace WebApplication_Sample.EF
{
    public static class DbContextIntializer
    {
        public static void Initialize(MyContext context, IHostingEnvironment env)
        {
            context.Database.Migrate();
            context.Database.EnsureCreated();

            var filePath = Path.Combine(env.ContentRootPath, @"wwwroot\InitialData.json");

            var usersAvailable = context.Users.Any();
            var policiesAvailable = context.UserPolicies.Any();

            if (!usersAvailable)
            {
                using (StreamReader r = new StreamReader(filePath))
                {
                    string json = r.ReadToEnd();
                    var items = JsonConvert.DeserializeObject<dynamic>(json);
                    foreach (dynamic item in items.users)
                    {
                        context.Users.Add(new User()
                        {
                            Key = item.key,
                            Email = item.email,
                            FullName = item.fullName,
                            Password = item.password,
                            Role = item.role
                        });
                    }
                    context.SaveChanges();
                }
            }

            if (!policiesAvailable)
            {
                using (StreamReader r = new StreamReader(filePath))
                {
                    string json = r.ReadToEnd();
                    var items = JsonConvert.DeserializeObject<dynamic>(json);
                    foreach (dynamic item in items.policies)
                    {
                        context.UserPolicies.Add(new UserPolicy()
                        {
                            Key = item.key,
                            Policy = item.policy,
                            UserKey = item.userKey
                        });
                    }
                    context.SaveChanges();
                }
            }
        }
    }
}