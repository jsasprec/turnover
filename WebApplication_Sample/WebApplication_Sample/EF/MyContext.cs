﻿using ClassLibrary_Sample;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace WebApplication_Sample.EF
{
    public class MyContext : DbContext
    {
        public MyContext(DbContextOptions<MyContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().HasIndex(x => new { x.Email}).IsUnique();
            modelBuilder.Entity<UserPolicy>().HasIndex(x => new { x.Policy, x.UserKey }).IsUnique();
        }

        public DbSet<Virus> Viruses { get; set; }
        public DbSet<User> Users{ get; set; }
        public DbSet<UserPolicy> UserPolicies{ get; set; }
    }
}