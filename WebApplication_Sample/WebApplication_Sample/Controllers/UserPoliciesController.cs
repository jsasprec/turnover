﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ClassLibrary_Sample;
using WebApplication_Sample.EF;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authorization;

namespace WebApplication_Sample.Controllers
{
    [Authorize(Policy = "policy")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserPoliciesController : ControllerBase
    {
        private readonly MyContext _context;

        public UserPoliciesController(MyContext context)
        {
            _context = context;
        }

        // GET: api/UserPolicies
        [HttpGet]
        [EnableQuery]
        public async Task<ActionResult<IEnumerable<UserPolicy>>> GetUserPolicies()
        {
            return await _context.UserPolicies.ToListAsync();
        }

        // GET: api/UserPolicies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserPolicy>> GetUserPolicy(string id)
        {
            var userPolicy = await _context.UserPolicies.FindAsync(id);

            if (userPolicy == null)
            {
                return NotFound();
            }

            return userPolicy;
        }

        // PUT: api/UserPolicies/5
        [HttpPut("{id}")]
        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> PutUserPolicy(string id, UserPolicy userPolicy)
        {
            if (id != userPolicy.Key)
            {
                return BadRequest();
            }

            _context.Entry(userPolicy).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserPolicyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/UserPolicies
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<UserPolicy>> PostUserPolicy(UserPolicy userPolicy)
        {
            _context.UserPolicies.Add(userPolicy);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUserPolicy", new { id = userPolicy.Key }, userPolicy);
        }

        // POST: api/UserPolicies
        [Authorize(Roles = "Admin")]
        [HttpPost("Bulk")]
        public async Task<ActionResult<UserPolicy>> PostUserPolicy_Bulk(ICollection<UserPolicy> userPolicies)
        {
            foreach(var userPolicy in userPolicies)
            {
                _context.UserPolicies.Add(userPolicy);
            }            
            await _context.SaveChangesAsync();

            return Ok(userPolicies);
        }

        // DELETE: api/UserPolicies/5
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<ActionResult<UserPolicy>> DeleteUserPolicy(string id)
        {
            var userPolicy = await _context.UserPolicies.FindAsync(id);
            if (userPolicy == null)
            {
                return NotFound();
            }

            _context.UserPolicies.Remove(userPolicy);
            await _context.SaveChangesAsync();

            return userPolicy;
        }

        private bool UserPolicyExists(string id)
        {
            return _context.UserPolicies.Any(e => e.Key == id);
        }
    }
}
