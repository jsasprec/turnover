﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ClassLibrary_Sample;
using WebApplication_Sample.EF;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authorization;

namespace WebApplication_Sample.Controllers
{
    [Authorize(Policy = "virus")]
    [Route("api/[controller]")]
    [ApiController]
    public class VirusController : ControllerBase
    {
        private readonly MyContext _context;

        public VirusController(MyContext context)
        {
            _context = context;
        }

        // GET: api/Virus
        [HttpGet]
        [EnableQuery]
        public async Task<ActionResult<IEnumerable<Virus>>> GetViruses()
        {
            return await _context.Viruses.ToListAsync();
        }

        // GET: api/Virus/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Virus>> GetVirus(string id)
        {
            var virus = await _context.Viruses.FindAsync(id);

            if (virus == null)
            {
                return NotFound();
            }

            return virus;
        }

        // PUT: api/Virus/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVirus(string id, Virus virus)
        {
            if (id != virus.Key)
            {
                return BadRequest();
            }

            _context.Entry(virus).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VirusExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Virus
        [HttpPost]
        public async Task<ActionResult<Virus>> PostVirus(Virus virus)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Viruses.Add(virus);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVirus", new { id = virus.Key }, virus);
        }

        // DELETE: api/Virus/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Virus>> DeleteVirus(string id)
        {
            var virus = await _context.Viruses.FindAsync(id);
            if (virus == null)
            {
                return NotFound();
            }

            _context.Viruses.Remove(virus);
            await _context.SaveChangesAsync();

            return virus;
        }

        private bool VirusExists(string id)
        {
            return _context.Viruses.Any(e => e.Key == id);
        }
    }
}
