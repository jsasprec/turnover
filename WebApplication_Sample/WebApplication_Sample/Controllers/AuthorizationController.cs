﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using WebApplication_Sample.EF;

namespace WebApplication_Sample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizationController : ControllerBase
    {
        private MyContext context;
        private IConfiguration iconfig;
        public AuthorizationController(MyContext _context, IConfiguration _iconfig)
        {
            context = _context;
            iconfig = _iconfig;
        }

        [HttpPost("SignIn")]
        public IActionResult SignIn(UserCredential cred)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userAvailable = context.Users.Any(x => x.Email.Equals(cred.Email));

            if (userAvailable)
            {
                var user = context.Users.FirstOrDefault(x => x.Email.Equals(cred.Email));
                if(!user.Password.Equals(cred.Password))
                {
                    return BadRequest(new { message = "Wrong Password" });
                }

                List<Claim> claim = new List<Claim>
                {
                    new Claim("UserKey", user.Key),
                    new Claim(JwtRegisteredClaimNames.Exp, DateTime.Now.AddDays(1).ToString())
                };

                var policies = context.UserPolicies.Where(x => x.UserKey.Equals(user.Key)).ToList();

                foreach (var item in policies)
                {
                    claim.Add(new Claim("policy", item.Policy));
                }

                claim.Add(new Claim("roles", user.Role));

                var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(iconfig["Jwt:jwt_key"]));
                
                var token = new JwtSecurityToken(
                                issuer: iconfig["Jwt:issuer"],
                                audience: iconfig["Jwt:audience"],
                                expires: DateTime.Now.AddDays(1),
                                claims: claim,
                                signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)
                                );

                var result = new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo,
                    created = DateTime.Now
                };
                
                return Ok(result);
            }
            else
                return BadRequest(new { message = "User Not Exist" });

        }

    }

    public class UserCredential
    {

        [Required]
        [MaxLength(120, ErrorMessage = "Maximum of 120 Characters.")]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }

}