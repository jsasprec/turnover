﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ClassLibrary_Sample
{
    public class User
    {
        public User()
        {
            Key = Key ?? Guid.NewGuid().ToString("N");
        }

        [Key]
        public string Key { get; set; }

        [Required]
        public string FullName { get; set; }

        [Required]
        [MaxLength(120, ErrorMessage = "Maximum of 120 Characters.")]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string Role { get; set; } //Admin, User
    }

    public class UserPolicy
    {
        public UserPolicy()
        {
            Key = Key ?? Guid.NewGuid().ToString("N");
        }

        [Key]
        public string Key { get; set; }

        [Required]
        public string Policy { get; set; }

        //Virus,User
        [Required]
        public string UserKey { get; set; } 
    }
}