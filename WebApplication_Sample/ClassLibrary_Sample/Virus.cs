﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClassLibrary_Sample
{
    public class Virus
    {
        public Virus()
        {
            Key = Key ?? Guid.NewGuid().ToString("N");
        }

        [Key]
        public string Key { get; set; }

        [Required(ErrorMessage ="Kelangan")]
        public string Title { get; set; }

        [Column(TypeName = "bit")]
        public bool IsActive { get; set; }
    }
}