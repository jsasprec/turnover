﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Globalization;

namespace ClassLibrary_Sample
{
    public partial class AppSettings
    {
        [JsonProperty("policyMapping")]
        public PolicyMapping[] PolicyMapping { get; set; }
    }

    public partial class PolicyMapping
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("policy")]
        public string[] Policy { get; set; }

        [JsonProperty("role")]
        public string[] Role { get; set; }
    }

    public partial class AppSettings
    {
        public static AppSettings FromJson(string json) => JsonConvert.DeserializeObject<AppSettings>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this AppSettings self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}